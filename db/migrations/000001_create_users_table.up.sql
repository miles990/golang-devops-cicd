
CREATE TABLE IF NOT EXISTS `users`(
   `user_id` serial PRIMARY KEY,
   `username` text NOT NULL,
   `password` text NOT NULL,
   `email` VARCHAR (100) UNIQUE NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;