package controller

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	// db "gogs.terpro.com/DDYNH/SMSGateway/db/mysql"
)

func (c *Controller) Hello(say string) {
	fmt.Println(say)
}

func (c *Controller) UserGET(ctx *gin.Context) {

	uid := ctx.Param("uid")

	var dbObj = map[string]interface{}{
		"uid": uid,
	}

	var datas = toSliceMap([]byte(toJSON(dbObj)))

	fmt.Println("ticket: ", toJSON(datas))

	ctx.JSON(http.StatusOK, datas)
	return
}
