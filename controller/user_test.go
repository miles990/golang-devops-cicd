package controller

import "testing"

func TestController_Hello(t *testing.T) {
	type args struct {
		say string
	}
	tests := []struct {
		name string
		c    *Controller
		args args
	}{
		// TODO: Add test cases.
		{
			args: args{"world"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.c.Hello(tt.args.say)
		})
	}
}
