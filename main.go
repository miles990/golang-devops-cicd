package main

import (
	"example.com/golang-devops-cicd/controller"	

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"

	"fmt"
	"os"
	"path"
)

// @contact.name Alex Lee
// @contact.email alexlee7171@gmail.com


func main() {

	loadConfig()

	var host = viper.GetString("host")
	var port = viper.GetString("port")
	if port == "" {
		port = "80"
	}

	

	// // Logging to a file.

	fmt.Println("debug: ", viper.GetBool("debug"))

	var router *gin.Engine
	if viper.GetBool("debug") {
		router = gin.Default()
		// router.Use(middleware.LoggerToFile())

	} else {
		gin.SetMode(gin.ReleaseMode)
		router = gin.New()
		router.Use(gin.Logger())
		router.Use(gin.Recovery())
	}

	// router.Use(middleware.LoggerToFile())

	// cors allows all origins
	router.Use(cors.Default())

	// route
	var c = controller.NewController()
	var v1 = router.Group("/api/v1")
	{
		var user = v1.Group("user")
		{
			user.GET("/:uid", c.UserGET)
		}
	}

	if viper.GetBool("showswagger") {
		router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
		fmt.Println(fmt.Sprintf("swagger: http://%s:%s/swagger/index.html", host, port))
	}

	router.Run(fmt.Sprintf(":%s", port))
}

func loadConfig() {
	viper.AutomaticEnv() // read in environment variables that match

	pwd, err := os.Getwd()
	if err != nil {
		fmt.Println(err.Error())
		// os.Exit(1)
	}
	var configPath = path.Join(pwd, "config")
	viper.SetConfigType("yaml")

	// config.yaml
	viper.SetConfigName("config")
	viper.AddConfigPath(configPath)
	fmt.Println("configPath: ", configPath)
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Load config.yaml: ", viper.ConfigFileUsed())
	} else {
		fmt.Println("Fatal error config file: ", err.Error())
	}

	// internal_config
	viper.SetConfigName("internal_config")
	viper.AddConfigPath(configPath)
	if err := viper.MergeInConfig(); err == nil {
		fmt.Println("Load internal_config.yaml: ", viper.ConfigFileUsed())
	}

}

//...
